package scg;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {

	public static Global global = new Global();

	public static void main(String[] args) {

		// load
		if (!global.load()) {
			System.out.println("Loading Error: " + global.getError());
			return;
		}

		// start
		global.start();
		
		// start Spring Boot
		SpringApplication app = new SpringApplication(Main.class);
		Map<String, Object> properties = new HashMap<>();
		properties.put("server.port", global.getPort());
//		properties.put("spring.servlet.multipart.max-file-size", global.getMaxFileSize());
//		properties.put("spring.servlet.multipart.max-request-size", global.getMaxFileSize());
		app.setDefaultProperties(properties);
		app.run(args);				
	}
}
