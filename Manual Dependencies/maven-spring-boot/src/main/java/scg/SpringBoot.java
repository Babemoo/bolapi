package scg;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringBoot {

	Global global = Main.global;

	@RequestMapping(value = "/", 
					    method = { RequestMethod.GET, RequestMethod.POST }, 
					    produces = "text/plain; charset=UTF-8")
	public String root() {
		
		return About.NAME + " " + About.VERSION + " (" + About.UPDATE + ")";
	}
	
	@RequestMapping(value = "/version", 
					    method = { RequestMethod.GET, RequestMethod.POST }, 
					    produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> version() {
		
		String result = "{ \"version\": \"" + About.VERSION + "\", \"update\": \"" + About.UPDATE + "\" }";
		return ResponseEntity.ok(result);
	}

//    @RequestMapping(value = "/")
//    public RedirectView  redirect() {
//        return new RedirectView("/index.html");
//    }	
//	
//	@RequestMapping(value = "/thai", method = GET, produces = "text/plain; charset=UTF-8")
//	public String thai(@RequestParam String id) {
//		return "ภาษาไทย | " + id;
//	}
//	
//	@RequestMapping(value = "/get", method = GET, produces = "application/json; charset=UTF-8")
//	public String get(@RequestParam String id) {
//
//		return "";
//	}
//
//	@RequestMapping(value = "/post", method = POST, produces = "application/json; charset=UTF-8")
//	public String post(@RequestParam String id, @RequestBody String json) {
//
//		return "";
//	}
//	
//	@RequestMapping(value = "/error500", method = GET, produces = "application/json; charset=UTF-8")
//	public ResponseEntity<String> error500() {
//		
//		return ResponseEntity.status(500).build();
//	}
//
//	@RequestMapping(value = "/ok200", method = GET, produces = "application/json; charset=UTF-8")
//	public ResponseEntity<String> ok200() {
//		
//		String result = "{ \"version\": \"" + Config.Version + "\", \"update\": \"" + Config.Update + "\" }";
//		return ResponseEntity.ok(result);
//	}

	private String _textToHtml(String text) {

		text = text.replaceAll("&", "&amp;");
		text = text.replaceAll("\\\\", "&#92;");
		text = text.replaceAll("\"", "&quot;");
		text = text.replaceAll("'", "&#39;");
		text = text.replaceAll("<", "&lt;");
		text = text.replaceAll(">", "&gt;");
		text = text.replaceAll("\r\n", "<br />");
		text = text.replaceAll("\n", "<br />");

		return text;
	}
}
