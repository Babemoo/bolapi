package com.example.ApiForBOL;

import static com.example.ApiForBOL.Global.gen;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;  

import java.util.Arrays;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.json.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import static java.time.LocalDate.now;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestTemplate;
import static org.springframework.web.servlet.function.RequestPredicates.headers;
 
///////////////////////////////////////////////////////////////////////////////////////////////

@RestController
@RequestMapping(value = "/")  
public class Controller  {    
   
        Global global = Main.global;
        private String appName;
        
        @GetMapping("/0/{year}/{companyId}")
        public Response0 CallController0(@PathVariable String year,@PathVariable String companyId,@RequestHeader HttpHeaders headers) throws IOException, InterruptedException {

                String status="0";
                String aa = global.getKey();
                List<String> bb = headers.get("Authorization");
                String result = bb.stream().map(n -> String.valueOf(n)).collect(Collectors.joining("", "", ""));
            
                if(aa.equals(result)){

                        Connection con1 = null;
                        Connection con2 = null;

                        try {

                                //Open a connection
                                con1 = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress1()+"/"+global.getDatabase1()+"", ""+global.getUser1()+"", ""+global.getPassword1()+"" );
                                con2 = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress2()+"/"+global.getDatabase2()+"", ""+global.getUser2()+"", ""+global.getPassword2()+"" );

                                System.out.println("Connected database successfully...");

                                String sql = "SELECT * FROM "+global.getDatabase1()+".boltable WHERE year = '"+year+"' and companyId = '"+companyId+"' ";
                                String sql2 = "SELECT * FROM "+global.getDatabase2()+".bol2table WHERE year = '"+year+"' and companyId = '"+companyId+"' ";
                                System.out.println("year :"+year);
                                
                                // create the java statement
                                Statement st = con1.createStatement();
                                Statement st2 = con2.createStatement();
                                
                                // execute the query, and get a java resultset
                                ResultSet rs = st.executeQuery(sql);
                                ResultSet rs2 = st2.executeQuery(sql2);

                                if (rs.next() == false || rs2.next() == false) {

                                        String Id = gen();

                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                                        TimeZone tz = TimeZone.getTimeZone("Asia/Bangkok"); 
                                        sdf.setTimeZone(tz); 

                                        java.util.Date date= new java.util.Date();
                                        Timestamp local = new Timestamp(date.getTime());
                                        String strDate = sdf.format(date);
                                        System.out.println("Local in String format " + strDate);

                                        try {

                                                Connection con = null;
                                                Statement stmt = null;

                                                con = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress3()+"/"+global.getDatabase3()+"", ""+global.getUser3()+"", ""+global.getPassword3()+"" );
                                                System.out.println("Connected database successfully...");

                                                String sql1 = "SELECT * from Queue where CompanyId = '"+companyId+"' and flag1 <2  and (status1 = 'waiting' OR status1 = 'unsuccess but BOL website is not locked')";// and dateTime BETWEEN timestamp(DATE_SUB(NOW(), INTERVAL 2 MINUTE)) AND timestamp(NOW())
                                                Statement st1 = con.createStatement();
                                                
                                                // execute the query, and get a java resultset
                                                ResultSet rs1 = st1.executeQuery(sql1);

                                                if (rs1.next() == false) {
                                                        try {

                                                                con = null;
                                                                stmt = null;

                                                                //Open a connection
                                                                con = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress3()+"/"+global.getDatabase3()+"", ""+global.getUser3()+"", ""+global.getPassword3()+"" );
                                                                System.out.println("Connected database successfully...");

                                                                //Execute a query
                                                                stmt = con.createStatement();
                                                                sql = "INSERT INTO Queue (id, year, status1, flag1 , CompanyId, dateTime) VALUES ('"+Id+"','"+year+"','waiting',0,'"+companyId+"','"+strDate+"')";
                                                                stmt.executeUpdate(sql);
                                                                System.out.println("Insert Success!");

                                                                con.close();
                                                                global.statusCall = "1";
                                                                status = Id;
                                                                System.out.println("Running "+global.statusCall); 

                                                                Response0 response0 = new Response0(
                                                                        status
                                                                );

                                                                return response0;

                                                        }catch(SQLException ex) {

                                                        // handle any errors
                                                        System.out.println("SQLException: " + ex.getMessage());
                                                        System.out.println("SQLState: " + ex.getSQLState());
                                                        System.out.println("VendorError: " + ex.getErrorCode());

                                                        }

                                                }else{

                                                        status = "1";
                                                        System.out.println("On Running"); 

                                                        Response0 response0 = new Response0(
                                                                status
                                                        );

                                                        st.close();
                                                        return response0;

                                                }

                                        }catch(SQLException ex) {

                                                // handle any errors
                                                System.out.println("SQLException: " + ex.getMessage());
                                                System.out.println("SQLState: " + ex.getSQLState());
                                                System.out.println("VendorError: " + ex.getErrorCode());

                                        }

                                        st.close();

                                }else {

                                        status = "0";
                                        System.out.println("Have data already"); 
                                        System.out.println("status:0");

                                        Response0 response0 = new Response0(
                                                status
                                        );

                                        st.close();
                                        return response0;
                                }

                        }catch (SQLException ex) {

                                // handle any errors
                                System.out.println("SQLException: " + ex.getMessage());
                                System.out.println("SQLState: " + ex.getSQLState());
                                System.out.println("VendorError: " + ex.getErrorCode());

                        } 

                }else{

                    System.out.println("Error");

                }

                Response0 response0 = new Response0("");
                return response0;

        }
    
        @GetMapping("/{Id}")
        public Response3 CallController3(@PathVariable String Id,@RequestHeader HttpHeaders headers)  {

                String aa = global.getKey();
                List<String> bb = headers.get("Authorization");
                String result = bb.stream().map(n -> String.valueOf(n)).collect(Collectors.joining("", "", ""));

                if(aa.equals(result)){

                        System.out.println("Yes");
                        Connection conn = null;  

                        try {  

                                //Open a connection
                                conn = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress3()+"/"+global.getDatabase3()+"", ""+global.getUser3()+"", ""+global.getPassword3()+"" );


                                System.out.println("Connected database successfully...");
                                System.out.println("");

                                String sql = "SELECT * FROM Queue where id = '"+Id+"'";

                                // create the java statement
                                Statement st = conn.createStatement();

                                // execute the query, and get a java resultset
                                ResultSet rs = st.executeQuery(sql);

                                if (rs.next() == false) {

                                        System.out.println("ResultSet in empty in Java"); 

                                }else {

                                        String status1 = rs.getString("status1");
                                        //String status2 = rs.getString("status2");

                                        String status;

                                        if(status1.equals("success")){

                                                status = "Success";

                                        }else if(status1.equals("unsuccess and BOL website is locked!")){

                                                status = "Unsuccess and BOL website is lock";

                                        }else if(status1.equals("waiting")){

                                                status = "Waiting";

                                        }else{

                                                status = "Unsuccess but BOL website isn't lock";

                                        }

                                        System.out.println("Status: "+status);

                                        Response3 response3 = new Response3(
                                                status
                                        );

                                        return response3;
                                }
                        }

                        catch (SQLException ex) {

                            // handle any errors
                            System.out.println("SQLException: " + ex.getMessage());
                            System.out.println("SQLState: " + ex.getSQLState());
                            System.out.println("VendorError: " + ex.getErrorCode());

                        }
                }

                Response3 response3 = new Response3("");
                return response3;
        } 

        @GetMapping("/1/{year}/{companyId}")
        public Response1 CallController1(@PathVariable String year,@PathVariable String companyId,@RequestHeader HttpHeaders headers) {

                String aa = global.getKey();
                List<String> bb = headers.get("Authorization");
                String result = bb.stream().map(n -> String.valueOf(n)).collect(Collectors.joining("", "", ""));

                if(aa.equals(result)){

                        System.out.println("Yes");
                        Connection conn = null;

                        try {  
                                //Open a connection
                                conn = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress1()+"/"+global.getDatabase1()+"", ""+global.getUser1()+"", ""+global.getPassword1()+"" );

                                System.out.println("Connected database successfully...");
                                System.out.println("");

                                String sql = "SELECT * FROM "+global.getDatabase1()+".boltable WHERE year = '"+year+"' and companyId = '"+companyId+"' ";

                                // create the java statement
                                Statement st = conn.createStatement();

                                // execute the query, and get a java resultset
                                ResultSet rs = st.executeQuery(sql);

                                if (rs.next() == false) {

                                        System.out.println("ResultSet in empty in Java"); 

                                }else {

                                        do { 

                                                String date = rs.getString("date");

                                                String cashAndDeposit = rs.getString("cashAndDeposit");
                                                String accountRecv = rs.getString("accountRecv");
                                                String accountRecvAndNoteRecv = rs.getString("accountRecvAndNoteRecv");
                                                String totalShortTermLoanRecv = rs.getString("totalShortTermLoanRecv");
                                                String inventory = rs.getString("inventory");

                                                String otherCurrentAssets = rs.getString("otherCurrentAssets");
                                                String totalCurAsset = rs.getString("totalCurAsset");
                                                String loanAndInvestment = rs.getString("loanAndInvestment");
                                                String propPlantEquip = rs.getString("propPlantEquip");
                                                String otherNonCurrentAssets = rs.getString("otherNonCurrentAssets");

                                                String totalNonCurrentAssets = rs.getString("totalNonCurrentAssets");
                                                String totalAsset = rs.getString("totalAsset");
                                                String bankOdAndLoan = rs.getString("bankOdAndLoan");
                                                String accountPayable = rs.getString("accountPayable");
                                                String totalAccPayableAndBillPaid = rs.getString("totalAccPayableAndBillPaid");

                                                String totalShortTermLoan = rs.getString("totalShortTermLoan");
                                                String otherCurLib = rs.getString("otherCurLib");
                                                String totalCurLib = rs.getString("totalCurLib");
                                                String longTermLoan = rs.getString("longTermLoan");
                                                String otherLib = rs.getString("otherLib");

                                                String totalNonCurLib = rs.getString("totalNonCurLib");
                                                String totalLib = rs.getString("totalLib");
                                                String authorizedPreferShare = rs.getString("authorizedPreferShare");
                                                String authorizedShareCapt = rs.getString("authorizedShareCapt");
                                                String paidInCaptPreferShare = rs.getString("paidInCaptPreferShare");

                                                String paidInCaptCommonStk = rs.getString("paidInCaptCommonStk");
                                                String retainedEarnings = rs.getString("retainedEarnings");
                                                String other = rs.getString("other");
                                                String totalEquity = rs.getString("totalEquity");
                                                String totalLibAndEquity = rs.getString("totalLibAndEquity");

                                                // print the results

                                                System.out.println("companyId"+companyId);
                                                System.out.println("year"+year);
                                                System.out.println("date"+date);

                                                System.out.println("cashAndDeposit"+cashAndDeposit);
                                                System.out.println("accountRecv"+accountRecv);
                                                System.out.println("accountRecvAndNoteRecv"+accountRecvAndNoteRecv);
                                                System.out.println("totalShortTermLoanRecv"+totalShortTermLoanRecv);
                                                System.out.println("inventory"+inventory);

                                                System.out.println("otherCurrentAssets"+otherCurrentAssets);
                                                System.out.println("totalCurAsset"+totalCurAsset);
                                                System.out.println("loanAndInvestment"+loanAndInvestment);
                                                System.out.println("propPlantEquip"+propPlantEquip);
                                                System.out.println("otherNonCurrentAssets"+otherNonCurrentAssets);

                                                System.out.println("totalNonCurrentAssets"+totalNonCurrentAssets);
                                                System.out.println("totalAsset"+totalAsset);
                                                System.out.println("bankOdAndLoan"+bankOdAndLoan);
                                                System.out.println("accountPayable"+accountPayable);
                                                System.out.println("totalAccPayableAndBillPaid"+totalAccPayableAndBillPaid);

                                                System.out.println("totalShortTermLoan"+totalShortTermLoan);
                                                System.out.println("otherCurLib"+otherCurLib);
                                                System.out.println("totalCurLib"+totalCurLib);
                                                System.out.println("longTermLoan"+longTermLoan);
                                                System.out.println("otherLib"+otherLib);

                                                System.out.println("totalNonCurLib"+totalNonCurLib);
                                                System.out.println("totalLib"+totalLib);
                                                System.out.println("authorizedPreferShare"+authorizedPreferShare);
                                                System.out.println("authorizedShareCapt"+authorizedShareCapt);
                                                System.out.println("paidInCaptPreferShare"+paidInCaptPreferShare);

                                                System.out.println("paidInCaptCommonStk"+paidInCaptCommonStk);
                                                System.out.println("retainedEarnings"+retainedEarnings);
                                                System.out.println("other"+other);
                                                System.out.println("totalEquity"+totalEquity);
                                                System.out.println("totalLibAndEquity"+totalLibAndEquity);

                                                System.out.println("");

                                                //return (inventory);
                                                Response1 response1 = new Response1(

                                                        companyId,
                                                        year,
                                                        date,
                                                        cashAndDeposit,
                                                        accountRecv,
                                                        accountRecvAndNoteRecv, 
                                                        totalShortTermLoanRecv, 
                                                        inventory, 
                                                        otherCurrentAssets, 
                                                        totalCurAsset,  
                                                        loanAndInvestment,  
                                                        propPlantEquip,  
                                                        otherNonCurrentAssets, 
                                                        totalNonCurrentAssets,  
                                                        totalAsset, 
                                                        bankOdAndLoan, 
                                                        accountPayable,  
                                                        totalAccPayableAndBillPaid, 
                                                        totalShortTermLoan, 
                                                        otherCurLib, 
                                                        totalCurLib,  
                                                        longTermLoan,
                                                        otherLib, 
                                                        totalNonCurLib,  
                                                        totalLib, 
                                                        authorizedPreferShare,  
                                                        authorizedShareCapt,  
                                                        paidInCaptPreferShare,  
                                                        paidInCaptCommonStk,  
                                                        retainedEarnings, 
                                                        other, 
                                                        totalEquity, 
                                                        totalLibAndEquity
                                                );

                                                return response1;
                                        }while (rs.next());
                                }

                                st.close();

                        }catch (SQLException ex) {

                                // handle any errors
                                System.out.println("SQLException: " + ex.getMessage());
                                System.out.println("SQLState: " + ex.getSQLState());
                                System.out.println("VendorError: " + ex.getErrorCode());
                        }

                        System.out.println(headers.get("Authorization")+"JJJ"+aa+" "+result+" ");
                        System.out.println(global.getKey());

                }else{

                    System.out.println("Error");

                }

                Response1 response1 = new Response1("","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","");
                return response1;

        } 

        @GetMapping("/2/{year}/{companyId}")
        public Response2 CallController2(@PathVariable String year,@PathVariable String companyId,@RequestHeader HttpHeaders headers) {

                String aa = global.getKey();
                List<String> bb = headers.get("Authorization");
                String result = bb.stream().map(n -> String.valueOf(n)).collect(Collectors.joining("", "", ""));

                    if(aa.equals(result)){

                        System.out.println("Yes");
                        Connection conn = null;

                        try {

                                //Open a connection
                                conn = DriverManager.getConnection( "jdbc:mysql://"+global.getAddress2()+"/"+global.getDatabase2()+"", ""+global.getUser2()+"", ""+global.getPassword2()+"" );

                                System.out.println("Connected database successfully...");
                                System.out.println("");

                                String sql = "SELECT * FROM "+global.getDatabase2()+".bol2table WHERE year = '"+year+"' and companyId = '"+companyId+"' ";

                                // create the java statement
                                Statement st = conn.createStatement();

                                // execute the query, and get a java resultset
                                ResultSet rs = st.executeQuery(sql);

                                if (rs.next() == false) {

                                        System.out.println("ResultSet in empty in Java"); 

                                }else {

                                        do { 

                                                String date = rs.getString("date");

                                                String totalrevenue = rs.getString("totalrevenue");
                                                String otherincome = rs.getString("otherincome");
                                                String costgoodsold = rs.getString("costgoodsold");
                                                String grossprofit = rs.getString("grossprofit");
                                                String operateexpense = rs.getString("operateexpense");

                                                String ebit = rs.getString("ebit");
                                                String otherexpense = rs.getString("otherexpense");
                                                String incomebeforetax = rs.getString("incomebeforetax");
                                                String interest = rs.getString("interest");
                                                String incometax = rs.getString("incometax");

                                                String netprofit = rs.getString("netprofit");

                                                // print the results

                                                System.out.println("companyId"+companyId);
                                                System.out.println("year"+year);
                                                System.out.println("date"+date);

                                                System.out.println("totalrevenue"+totalrevenue);
                                                System.out.println("otherincome"+otherincome);
                                                System.out.println("costgoodsold"+costgoodsold);
                                                System.out.println("grossprofit"+grossprofit);
                                                System.out.println("operateexpense"+operateexpense);

                                                System.out.println("ebit"+ebit);
                                                System.out.println("otherexpense"+otherexpense);
                                                System.out.println("incomebeforetax"+incomebeforetax);
                                                System.out.println("interest"+interest);
                                                System.out.println("incometax"+incometax);

                                                System.out.println("netprofit"+netprofit);

                                                System.out.println("");

                                                //return (inventory);
                                                Response2 response2 = new Response2(
                                                        companyId,
                                                        year,
                                                        date,
                                                        totalrevenue,
                                                        otherincome,
                                                        costgoodsold,
                                                        grossprofit,
                                                        operateexpense,
                                                        ebit,
                                                        otherexpense,
                                                        incomebeforetax,
                                                        interest,
                                                        incometax,
                                                        netprofit
                                                );

                                                return response2;

                                        }while (rs.next());
                                }

                                st.close();

                        }catch (SQLException ex) {

                                // handle any errors
                                System.out.println("SQLException: " + ex.getMessage());
                                System.out.println("SQLState: " + ex.getSQLState());
                                System.out.println("VendorError: " + ex.getErrorCode());
                        }

                }else{

                        System.out.println("Error");

                }

                Response2 response2 = new Response2("","","","","","","","","","","","","","");
                return response2;

        }
}