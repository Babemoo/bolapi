package com.example.ApiForBOL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

public class Global {
 
        private final String CONFIG_PATH ="/home/autofsservice/API/src/main/config.json";
        volatile static String statusCall ="1";
        static String ID;
        static String CompanyId,statusEmpthy="0",theId;

        public static String gen() {
        
                Random r = new Random( System.currentTimeMillis() );
                ID= String.valueOf(10000 + r.nextInt(20000));
                System.out.println(ID);
                return ID;
        }
    
        public boolean load() {
        
                JSONObject root = alisa.json.Parser.loadObject(CONFIG_PATH);
	if (root == null) {
                        this._error = "parsing error";
                        return false;   
	}
        
                // version
	try {
                        String version = root.getString("version");
                        if (!version.equals("3.0")) {
                                this._error = "incorrect version (" + version + ")";
		return false;
                        }
	}catch (JSONException ex) {
                         this._error = "version";
                         return false;
                }

	// port 
	try {
                        this._port = root.getInt("port");
	}catch (JSONException ex) {
                        this._error = "port";
                        return false;
	}

	// key 
	try {
                        this._key = root.getString("key");
	}catch (JSONException ex) {
                        this._error = "key";
                        return false;
	}
        
                try {
                        this._apiDelay = root.getString("api_delay");
	}catch (JSONException ex) {
                        this._error = "api_delay";
                        return false;
	}
                
                try {
                        this._DBexecuteDelay = root.getString("DBexecute_delay");
	}catch (JSONException ex) {
                        this._error = "DBexecuteDelay";
                        return false;
	}
                /////////////////////////////////////////////////////////////////	
	// database1 
                try {
                         JSONObject sql = root.getJSONObject("database1");		
		
                        // address (String)
                        try {
                                this._address1 = sql.getString("address1");
                        }catch (JSONException ex) {
                                this._error = "address1";
                                return false;
                        }
		
                        // database (String)
                        try {
                                this._database1 = sql.getString("database1");
                        }catch (JSONException ex) {
                                this._error = "database1";
                                return false;
                        }

                        // user (String)
                        try {
                                this._user1 = sql.getString("user1");
                        }catch (JSONException ex) {
                                this._error = "user1";
                                return false;
                        }
 
                        // password (String)
                        try {
                                this._password1 = sql.getString("password1");
                        }catch (JSONException ex) {
                                this._error = "password1";
                                return false;
                        }            
                        
                }catch (JSONException ex) {
                        this._error = "database1";
                        return false;
                }      
    
                /////////////////////////////////////////////////////////////////
                // database2 (Object)
                try {
                        JSONObject sql = root.getJSONObject("database2");		
		
                        // address (String)
                        try {
                                this._address2 = sql.getString("address2");
                        }catch (JSONException ex) {
                                this._error = "address2";
                                return false;
                        }
		
                        // database (String)
                        try {
                                this._database2 = sql.getString("database2");
                        }catch (JSONException ex) {
                                this._error = "database2";
                                return false;
                        }

                        // user (String)
                        try {
                                this._user2 = sql.getString("user2");
                        }
                        catch (JSONException ex) {
                                this._error = "user2";
                                return false;
                        }
 
                        // password (String)
                        try {
                                this._password2 = sql.getString("password2");
                        }
                        catch (JSONException ex) {
                                this._error = "password2";
                                return false;
                        }      
                        
                }catch (JSONException ex) {
                        this._error = "database2";
                        return false;
                }  
                
                /////////////////////////////////////////////////////////////////
                // database3 (Object)
                try {
                        JSONObject sql = root.getJSONObject("database3");		

                        // address (String)
                        try {
                                this._address3 = sql.getString("address3");
                        }
                        catch (JSONException ex) {
                                this._error = "address3";
                                return false;
                        }
		
                        // database (String)
                         try {
                                this._database3 = sql.getString("database3");
                         }
                         catch (JSONException ex) {
                                this._error = "database3";
                                return false;
                         }

                        // user (String)
                        try {
                                this._user3 = sql.getString("user3");
                        }
                        catch (JSONException ex) {
                                this._error = "user3";
                                return false;
                        }
 
                        // password (String)
                        try {
                                this._password3 = sql.getString("password3");
                        }
                        catch (JSONException ex) {
                                this._error = "password3";
                                return false;
                        }        
                        
                }catch (JSONException ex) {
                        this._error = "database3";
                        return false;
                }      
                
                return true;
        }   
    
        public void start() throws InterruptedException {
            
                do{
                        Connection connection = null;

                        try {

                                // Load the MySQL JDBC driver
                                String driverName = "com.mysql.cj.jdbc.Driver";
                                Class.forName(driverName);

                                // Create a connection to the database
                                connection = DriverManager.getConnection( "jdbc:mysql://"+getAddress3()+"/"+getDatabase3()+"", ""+getUser3()+"", ""+getPassword3()+"" );
                                System.out.println("Successfully Connected to the database!");

                        }catch (ClassNotFoundException e) {
                                System.out.println("Could not find the database driver " + e.getMessage());

                        }catch (SQLException e) {
                                System.out.println("Could not connect to the database " + e.getMessage());
                        }
                
                         try {

                                // Get a result set containing all data from test_table

                                Statement statement = connection.createStatement();
                                ResultSet results = statement.executeQuery("SELECT * from Queue where flag1 <2  and (status1 = 'waiting'  OR status1 = 'unsuccess but BOL website is not locked')");

                                while (results.next()) { //have data

                                        try {  

                                                ProcessBuilder processBuilder = new ProcessBuilder(); 
                                                processBuilder.redirectErrorStream(true);
                                                processBuilder.command("bash", "-c", "/usr/bin/java -jar ~/BOLCodeProject/target/BOLCodeProject-1.0-SNAPSHOT.jar");//
                                                Process p = processBuilder.start();

                                                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                                String line;

                                                while ((line = reader.readLine()) != null) {
                                                        System.out.println("tasklist: " + line);
                                                }

                                                p.waitFor();
                                                System.out.println ("BOL1 exitValue of Process1: " + p.exitValue());

                                                p.destroy();

                                        }catch (Exception e) {
                                                System.out.println("Could not retrieve data from the database " + e.getMessage());
                                        }

                                        int n=Integer.parseInt(getDBexecuteDelay());  
                                        System.out.println("getDBexecuteDelay = "+n);
                                        
                                        for(int i=0;i<n;i++){

                                                TimeUnit.SECONDS.sleep(1);
                                                //System.out.println(i+" sleep");

                                                statement = connection.createStatement();
                                                results = statement.executeQuery("SELECT * from Queue where flag1 <2  and (status1 = 'waiting'  OR status1 = 'unsuccess but BOL website is not locked')");

                                        }
                                }
                    
                                statusCall ="0";

                        }catch (SQLException e) {
                                System.out.println("Have no data in the database " + e.getMessage());
                        }

                        System.out.println("Before enter while loop : "+ statusCall);
                        
                        while(true){

                                if(statusCall == "1"){
                                        break;
                                }
                                
                                int n=Integer.parseInt(getApiDelay());  
                                //System.out.println("getApiDelay = "+n);
                                 
                                for(int i=0;i<n;i++){

                                                TimeUnit.SECONDS.sleep(1);
                                                //System.out.println(i+" sleep in little loop");

                               }
                        }
                      
                }while(true);
        }

    public int getPort() {
            return this._port;
    }

    private int _port = -1;

    public String getError() {
            return this._error;   
    }

    private String _error = "";

    //////////////////////////////////////
    public String getApiDelay() {
            return this._apiDelay;
    }

    private String _apiDelay = "";

    public String getDBexecuteDelay() {
            return this._DBexecuteDelay;   
    }

    private String _DBexecuteDelay = "";

    //////////////////////////////////////
    public String getUser1() {
            return this._user1;   
    }

    private String _user1 = "";

    public String getPassword1() {
            return this._password1;   
    }

    private String _password1 = "";

    public String getKey() {
            return this._key;   
    }

    private String _key = "";

    public String getAddress1() {
           return this._address1;
    }

    private String _address1 = "";

    public String getDatabase1() {
           return this._database1;   
    }

    private String _database1 = "";

    //////////////////////////////////////
    public String getUser2() {
            return this._user2;   
    }

    private String _user2 = "";

    public String getPassword2() {
            return this._password2;   
    }

    private String _password2 = "";

    public String getAddress2() {
           return this._address2;
    }

    private String _address2 = "";

    public String getDatabase2() {
           return this._database2;   
    }

    private String _database2 = "";

    //////////////////////////////////////
    public String getUser3() {
            return this._user3;   
    }

    private String _user3 = "";

    public String getPassword3() {
            return this._password3;   
    }

    private String _password3 = "";

    public String getAddress3() {
           return this._address3;
    }

    private String _address3 = "";

    public String getDatabase3() {
           return this._database3;   
    }

    private String _database3 = "";

    boolean validate(String key) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

