package com.example.ApiForBOL;
import java.util.Arrays;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.json.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


public class Response1 {
    
        private String companyId;
        private String year;
        private String date;

        private String cashAndDeposit;
        private String accountRecv;
        private String accountRecvAndNoteRecv;
        private String totalShortTermLoanRecv;
        private String inventory;
        private String otherCurrentAssets;
        private String totalCurAsset;
        private String loanAndInvestment;
        private String propPlantEquip;
        private String otherNonCurrentAssets;
        private String totalNonCurrentAssets;
        private String totalAsset;
        private String bankOdAndLoan;
        private String accountPayable;
        private String totalAccPayableAndBillPaid;
        private String totalShortTermLoan;
        private String otherCurLib;
        private String totalCurLib;
        private String longTermLoan;
        private String otherLib;
        private String totalNonCurLib;
        private String totalLib;
        private String authorizedPreferShare;
        private String authorizedShareCapt;
        private String paidInCaptPreferShare;
        private String paidInCaptCommonStk;
        private String retainedEarnings;
        private String other;
        private String totalEquity;
        private String totalLibAndEquity;

  
   
        public Response1(
                String companyId,
                String year,
                String date,

                String cashAndDeposit, //1
                String accountRecv, //2
                String accountRecvAndNoteRecv, //3
                String totalShortTermLoanRecv, //4
                String inventory, //5
                String otherCurrentAssets, //6
                String totalCurAsset, //7
                String loanAndInvestment, //8
                String propPlantEquip, //9
                String otherNonCurrentAssets, //10
                String totalNonCurrentAssets, //11
                String totalAsset, //12
                String bankOdAndLoan, //13
                String accountPayable, //14
                String totalAccPayableAndBillPaid, //15
                String totalShortTermLoan, //16
                String otherCurLib, //17
                String totalCurLib, //18
                String longTermLoan, //19
                String otherLib, //20
                String totalNonCurLib, //21
                String totalLib, //22
                String authorizedPreferShare, //23
                String authorizedShareCapt, //24
                String paidInCaptPreferShare, //25
                String paidInCaptCommonStk, //26
                String retainedEarnings, //27
                String other, //28
                String totalEquity, //29
                String totalLibAndEquity){ //30

                this.companyId=companyId;
                this.year=year;
                this.date=date;

                this.cashAndDeposit=cashAndDeposit;
                this.accountRecv=accountRecv;
                this.accountRecvAndNoteRecv=accountRecvAndNoteRecv;
                this.totalShortTermLoanRecv=totalShortTermLoanRecv;
                this.inventory=inventory;
                this.otherCurrentAssets=otherCurrentAssets;
                this.totalCurAsset=totalCurAsset;
                this.loanAndInvestment=loanAndInvestment;
                this.propPlantEquip=propPlantEquip;
                this.otherNonCurrentAssets=otherNonCurrentAssets;
                this.totalNonCurrentAssets=totalNonCurrentAssets;
                this.totalAsset=totalAsset;
                this.bankOdAndLoan=bankOdAndLoan;
                this.accountPayable=accountPayable;
                this.totalAccPayableAndBillPaid=totalAccPayableAndBillPaid;
                this.totalShortTermLoan=totalShortTermLoan;
                this.otherCurLib=otherCurLib;
                this.totalCurLib=totalCurLib;
                this.longTermLoan=longTermLoan;
                this.otherLib=otherLib;
                this.totalNonCurLib=totalNonCurLib;
                this.totalLib=totalLib;
                this.authorizedPreferShare=authorizedPreferShare;
                this.authorizedShareCapt=authorizedShareCapt;
                this.paidInCaptPreferShare=paidInCaptPreferShare;
                this.paidInCaptCommonStk=paidInCaptCommonStk;
                this.retainedEarnings=retainedEarnings;
                this.other=other;
                this.totalEquity=totalEquity;
                this.totalLibAndEquity=totalLibAndEquity;
    
        }
   
        public String getcompanyId(){ 
                return companyId;
        }
    
        public String getyear(){ 
                return year;
        }
    
        public String getdate(){ 
                return date;
        }
    
        public String getcashAndDeposit(){ //1
                return cashAndDeposit;
        }
    
        public String getaccountRecv(){ //2
                return accountRecv;
        }
    
        public String getaccountRecvAndNoteRecv(){ //3
                return accountRecvAndNoteRecv;
        }
    
        public String gettotalShortTermLoanRecv(){ //4
                return totalShortTermLoanRecv;
        }
    
        public String getinventory(){ //5
                return inventory;
        }
    
        public String getotherCurrentAssets(){ //6
            return otherCurrentAssets;
        }

        public String gettotalCurAsset(){ //7
            return totalCurAsset;
        }

        public String getloanAndInvestment(){ //8
            return loanAndInvestment;
        }

        public String getpropPlantEquip(){ //9
            return propPlantEquip;
        }

        public String getotherNonCurrentAssets(){ //10
            return otherNonCurrentAssets;
        }

        public String gettotalNonCurrentAssets(){ //11
            return totalNonCurrentAssets;
        }

        public String gettotalAsset(){ //12
            return totalAsset;
        }

        public String getbankOdAndLoan(){ //13
            return bankOdAndLoan;
        }

        public String getaccountPayable(){ //14
            return accountPayable;
        }

        public String gettotalAccPayableAndBillPaid(){ //15
            return totalAccPayableAndBillPaid;
        }

        public String gettotalShortTermLoan(){ //16
            return totalShortTermLoan;
        }

        public String getotherCurLib(){ //17
            return otherCurLib;
        }

        public String gettotalCurLib(){  //18
            return totalCurLib;
        }

        public String getlongTermLoan(){ //19
            return longTermLoan;
        }

        public String getotherLib(){ //20
            return otherLib;
        }

        public String gettotalNonCurLib(){ //21
            return totalNonCurLib;
        }

        public String gettotalLib(){ //22
            return totalLib;
        }

        public String getauthorizedPreferShare(){  //23
            return authorizedPreferShare;
        }

        public String getauthorizedShareCapt(){ //24
            return authorizedShareCapt;
        }

        public String getpaidInCaptPreferShare(){ //25
            return paidInCaptPreferShare;
        }

        public String getpaidInCaptCommonStk(){ //26
            return paidInCaptCommonStk;
        }

        public String getretainedEarnings(){ //27
            return retainedEarnings;
        }

        public String getother(){ //28
            return other;
        }

        public String gettotalEquity(){ //29
            return totalEquity;
        }

        public String gettotalLibAndEquity(){ //30
            return totalLibAndEquity;
        }

        public void setcompanyId(String companyId){ 
                this.companyId=companyId;
        }

        public void setyear(String year){ 
                this.year=year;
        }

        public void setdate(String date){ 
                this.date=date;
        }

        public void setcashAndDeposit(String cashAndDeposit){ //1
                this.cashAndDeposit=cashAndDeposit;
        }

        public void setaccountRecv(String accountRecv){ //2
                this.accountRecv=accountRecv;
        }

        public void setaccountRecvAndNoteRecv(String accountRecvAndNoteRecv){ //3
                this.accountRecvAndNoteRecv=accountRecvAndNoteRecv;
        }

        public void settotalShortTermLoanRecv(String totalShortTermLoanRecv){ //4
                this.totalShortTermLoanRecv=totalShortTermLoanRecv;
        }

        public void setlastName(String inventory){ //5
                this.inventory=inventory;
        }

        public void setfirstName(String otherCurrentAssets){ //6
                this.otherCurrentAssets=otherCurrentAssets;
        }

        public void settotalCurAsset(String totalCurAsset){ //7
                this.totalCurAsset=totalCurAsset;
        }

        public void setloanAndInvestment(String loanAndInvestment){ //8
                this.loanAndInvestment=loanAndInvestment;
        }

        public void setpropPlantEquip (String propPlantEquip){ //9
                this.propPlantEquip=propPlantEquip;
        }

        public void setotherNonCurrentAssets(String otherNonCurrentAssets){ //10
                this.otherNonCurrentAssets=otherNonCurrentAssets;
        }

        public void settotalNonCurrentAssets(String totalNonCurrentAssets){ //11
                this.totalNonCurrentAssets=totalNonCurrentAssets;
        }

        public void settotalAsset(String totalAsset){ //12
                this.totalAsset=totalAsset;
        }

        public void setbankOdAndLoan(String bankOdAndLoan){ //13
                this.bankOdAndLoan=bankOdAndLoan;
        }

        public void setaccountPayable(String accountPayable){ //14
                this.accountPayable=accountPayable;
        }

        public void settotalAccPayableAndBillPaid(String totalAccPayableAndBillPaid){ //15
                this.totalAccPayableAndBillPaid=totalAccPayableAndBillPaid;
        }

        public void settotalShortTermLoan(String totalShortTermLoan){ //16
                this.totalShortTermLoan=totalShortTermLoan;
        }

        public void setotherCurLib(String otherCurLib){ //17
                this.otherCurLib=otherCurLib;
        }

        public void settotalCurLib(String totalCurLib){ //18
                this.totalCurLib=totalCurLib;
        }

        public void setlongTermLoan(String longTermLoan){ //19
                this.longTermLoan=longTermLoan;
        }

        public void setotherLib(String otherLib){ //20
                this.otherLib=otherLib;
        }

        public void settotalNonCurLib(String totalNonCurLib){ //21
                this.totalNonCurLib=totalNonCurLib;
        }

        public void settotalLib(String totalLib){ //22
                this.totalLib=totalLib;
        }

        public void setauthorizedPreferShare(String authorizedPreferShare){ //23
                this.authorizedPreferShare=authorizedPreferShare;
        }

        public void setauthorizedShareCapt(String authorizedShareCapt){ //24
                this.authorizedShareCapt=authorizedShareCapt;
        }

        public void setpaidInCaptPreferShare(String paidInCaptPreferShare){ //25
                this.paidInCaptPreferShare=paidInCaptPreferShare;
        }

        public void setpaidInCaptCommonStk(String paidInCaptCommonStk){ //26
                this.paidInCaptCommonStk=paidInCaptCommonStk;
        }

        public void setretainedEarnings(String retainedEarnings){ //27
                this.retainedEarnings=retainedEarnings;
        }

        public void setother(String other){ //28
                this.other=other;
        }

        public void setltotalEquity(String totalEquity){ //29
                this.totalEquity=totalEquity;
        }

        public void settotalLibAndEquity(String totalLibAndEquity){ //30
                this.totalLibAndEquity=totalLibAndEquity;
        }      
}
