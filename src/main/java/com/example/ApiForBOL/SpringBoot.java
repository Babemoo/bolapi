package com.example.ApiForBOL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.json.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;

@SpringBootApplication

public class SpringBoot {
    
        Global global = Main.global;

        public static void CallSpringBoot(String[] args) {
        
                SpringApplication.run(SpringBoot.class, args);
                
        }
}
