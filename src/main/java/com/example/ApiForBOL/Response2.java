package com.example.ApiForBOL;
import java.util.Arrays;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.json.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public class Response2 {

        static Object status(int i) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    
        private String companyId;
        private String year;
        private String date;
        private String totalrevenue;
        private String otherincome;
        private String costgoodsold;
        private String grossprofit;
        private String operateexpense;
        private String ebit;
        private String otherexpense;
        private String incomebeforetax;
        private String interest;
        private String incometax;
        private String netprofit;
    
        public Response2(
                
                String companyId,
                String year,
                String date,

                String totalrevenue, //1
                String otherincome, //2
                String costgoodsold, //3
                String grossprofit, //4
                String operateexpense, //5
                String ebit, //6
                String otherexpense, //7
                String incomebeforetax, //8
                String interest, //9
                String incometax, //10
                String netprofit //11
                
        ){ //14
            
                this.companyId=companyId;
                this.year=year;
                this.date=date;

                this.totalrevenue=totalrevenue;
                this.otherincome=otherincome;
                this.costgoodsold=costgoodsold;
                this.grossprofit=grossprofit;
                this.operateexpense=operateexpense;
                this.ebit=ebit;
                this.otherexpense=otherexpense;
                this.incomebeforetax=incomebeforetax;
                this.interest=interest;
                this.incometax=incometax;
                this.netprofit=netprofit;
                
        }
   
        public String getcompanyId(){ 
                return companyId;
        }
    
        public String getyear(){ 
                return year;
        }
    
        public String getdate(){ 
                return date;
        }
    
        public String gettotalrevenue(){ //1
                return totalrevenue;
        }
    
        public String getotherincome(){ //2
                return otherincome;
        }
    
        public String getcostgoodsold(){ //3
                return costgoodsold;
        }
    
        public String getgrossprofit(){ //4
                return grossprofit;
        }
    
        public String getoperateexpense(){ //5
                return operateexpense;
        }
    
        public String getebit(){ //6
                return ebit;
        }
    
        public String getotherexpense(){ //7
                return otherexpense;
        }
    
        public String getincomebeforetax(){ //8
                return incomebeforetax;
        }
    
        public String getinterest(){ //9
                return interest;
        }
    
        public String getincometax(){ //10
                return incometax;
        }
    
        public String getnetprofit(){ //11
                return netprofit;
        }
        
        public void setcompanyId(String companyId){ 
                this.companyId=companyId;
        }
    
        public void setyear(String year){ 
                this.year=year;
        }
    
        public void setdate(String date){ 
                this.date=date;
        }
   
        public void settotalrevenue(String totalrevenue){ //1
                this.totalrevenue=totalrevenue;
        }
    
        public void setotherincome(String otherincome){ //2
                this.otherincome=otherincome;
        }

        public void setcostgoodsold(String costgoodsold){ //3
                this.costgoodsold=costgoodsold;
        }

        public void setgrossprofit(String grossprofit){ //4
                this.grossprofit=grossprofit;
        }

        public void setoperateexpense(String operateexpense){ //5
                this.operateexpense=operateexpense;
        }
    
        public void setebit(String ebit){ //6
                this.ebit=ebit;
        }

        public void setotherexpense(String otherexpense){ //7
                this.otherexpense=otherexpense;
        }

        public void setincomebeforetax(String incomebeforetax){ //8
                this.incomebeforetax=incomebeforetax;
        }

        public void setpropinterest (String interest){ //9
                this.interest=interest;
        }

        public void setincometax(String incometax){ //10
                this.incometax=incometax;
        }

        public void setnetprofit(String netprofit){ //11
                this.netprofit=netprofit;
        }
              
}
